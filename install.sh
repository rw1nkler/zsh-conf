#!/bin/bash

# Options
SHOW_HELP=false
FORCE=false
VERBOSE=false

# Configuration variables
OH_MY_ZSH_DIR=~/.oh-my-zsh
ZSHRC_SOURCE=`pwd`/zshrc
ZSHRC_TARGET=~/.zshrc

while getopts ":hvf" opt; do
    case ${opt} in
        h )
            SHOW_HELP=true
            ;;
        f )
            FORCE=true
            ;;
        v )
            VERBOSE=true
            ;;
        \? )
            echo "Invalid option -${OPTARG}"
            exit 1
            ;;
    esac
done

if $VERBOSE; then
    exec 3>&1
else
    exec 3>/dev/null
fi

# Setup oh-my-zsh

if [[ -d $OH_MY_ZSH_DIR ]]; then
    echo "INFO: $OH_MY_ZSH directory already exists!" >&3
    if $FORCE; then
        echo "INFO: (FORCE) Reclonning oh-my-zsh repository" >&3
        rm -rf ~/.oh-my-zsh
        git clone --force https://github.com/robbyrussell/oh-my-zsh.git ${OH_MY_ZSH_DIR}
    fi
else
   echo "INFO: Clonning oh-my-zsh repository" >&3
   git clone https://github.com/robbyrussell/oh-my-zsh.git ${OH_MY_ZSH_DIR}
fi

# Make copy of local files (default)

if ! $FORCE; then
    if [[ -f $ZSHRC_TARGET ]] && [[ ! -h $ZSHRC_TARGET ]]; then
        echo "INFO: File ${ZSHRC_TARGET} already exists! Moving to ${ZSHRC_TARGET}.bak ..." >&3
        mv ${ZSHRC_TARGET} ${ZSHRC_TARGET}.bak

    elif [[ -h $ZSHRC_TARGET ]]; then
        LINK_TARGET=`readlink ~/.zshrc`
        echo "INFO: Symbolic link to ${LINK_TARGET} already exists! Moving to ${ZSHRC_TARGET}.bak" >&3
        mv ${ZSHRC_TARGET} ${ZSHRC_TARGET}.bak
    fi
    echo "INFO: Creating link to $ZSHRC_TARGET" >&3
    ln -s ${ZSHRC_SOURCE} ${ZSHRC_TARGET}
else
    echo "INFO: (FORCE) Creating link to $ZSHRC_TARGET" >&3
    ln -s -f ${ZSHRC_SOURCE} ${ZSHRC_TARGET}
fi

echo "Setting ZSH as shell demends root permissions:"
chsh -s $(which zsh)

