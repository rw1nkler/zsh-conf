#!/bin/bash

# Options
SHOW_HELP=flase
VERBOSE=false

# Configuration variables
OH_MY_ZSH_DIR=~/.oh-my-zsh
ZSHRC_TARGET=~/.zshrc

while getopts ":hv" opt; do
    case ${opt} in
        h )
            SHOW_HELP=true
            ;;
        v )
            VERBOSE=true
            ;;
        \? )
            echo "Invalid option -${OPTARG}"
            exit 1
            ;;
    esac
done

if $VERBOSE; then
    exec 3>&1
else
    exec 3>/dev/null
fi

# Remove oh-my-zsh

if [[ -d $OH_MY_ZSH_DIR ]]; then
    echo "INFO: Removing $OH_MY_ZSH_DIR" >&3
    rm -rf $OH_MY_ZSH_DIR
fi

if [[ -h $ZSHRC_TARGET ]]; then
    LINK_TARGET=`readlink $ZSHRC_TARGET`
    echo "INFO: Removing $ZSHRC_TARGET (LINK: $LINK_TARGET)" >&3
fi

